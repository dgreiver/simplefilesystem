#include<stdio.h>
#include <math.h>
#define USE_MATH_DEFINES
#define SURFACE(x) 6*x*x;
#define LEFTOVER(r, h) M_PI*(r**2)*h*2/3;
#define NORMAL(vector) 0x5f3759df - (vector >> 1);

